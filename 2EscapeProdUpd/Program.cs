﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using ShopifySharp;
using ShopifySharp.Filters;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Numeric;
using System.Text.RegularExpressions;

namespace _2EscapeProdUpd
{
    class Program
    {

        public static IConfigurationRoot config { get; set; }

        private static ExcelWorksheet sheet;
        private static bool[] tagsbool = new bool[34];
        private static ProductOption[] prodOptions = new ProductOption[3];
        private static Product product = null;
        private static ProductService service;

        private static int FindWorksheet(ExcelPackage infile, string worksheetName)
    {
        var worksheetCounts = infile.Workbook.Worksheets.Count;
        var found = false;
        int i = 1;

        while (!found && i <= worksheetCounts)
        {
            if (infile.Workbook.Worksheets[i].Name.ToUpper() == worksheetName.ToUpper())
            {
                found = true;
            }
            else
            {
                i += 1;
            }
        }

        if (!found)
        {
            Console.WriteLine($"Cannot find worksheet {worksheetName}");
            i = 0;
        }
        return i;
    }

        private static string settags(string bType)
        {
            string tags = "";

            if (tagsbool[0]) tags = "road";
            if (tagsbool[1]) tags = tags + ",lifestyle";
            if (tagsbool[2]) tags = tags + ",triathlon";
            if (tagsbool[3]) tags = tags + ",xc";
            if (tagsbool[4]) tags = tags + ",enduro";
            if (tagsbool[5]) tags = tags + ",trail";
            if (tagsbool[6]) tags = tags + ",gravity";
            if (tagsbool[7]) tags = tags + ",gravel";
            if (tagsbool[8]) tags = tags + ",cyclocross";
            if (tagsbool[9]) tags = tags + ",male";
            if (tagsbool[10]) tags = tags + ",female";
            if (tagsbool[11]) tags = tags + ",boys";
            if (tagsbool[12]) tags = tags + ",girls";
            if (tagsbool[13]) tags = tags + ",aluminium";
            if (tagsbool[14]) tags = tags + ",steel";
            if (tagsbool[15]) tags = tags + ",titanium";
            if (tagsbool[16]) tags = tags + ",carbon fiber";
            if (tagsbool[17]) tags = tags + ",red";
            if (tagsbool[18]) tags = tags + ",green";
            if (tagsbool[19]) tags = tags + ",blue";
            if (tagsbool[20]) tags = tags + ",yellow";
            if (tagsbool[21]) tags = tags + ",orange";
            if (tagsbool[22]) tags = tags + ",black";
            if (tagsbool[23]) tags = tags + ",white";
            if (tagsbool[24]) tags = tags + ",purple";
            if (tagsbool[25]) tags = tags + ",gray";
            if (tagsbool[26]) tags = tags + ",pink";
            if (tagsbool[27]) tags = tags + ",silver";
            if (tagsbool[28]) tags = tags + ",cyan";
            if (tagsbool[29]) tags = tags + ",gold";
            if (tagsbool[30]) tags = tags + ",brown";
            if (tagsbool[31]) tags = tags + ",maroon";

            if (!String.IsNullOrEmpty(bType)) tags = tags + ",twoesc-dealer_discipline_" + bType;

            return tags;
        }
        
        
        private static void checkTags(int row)

        {
            string x = "";
            
            for (var a = 0; a < 34; a++)
                {
                if (sheet.Cells[row, 17 + a].Value != null)  x = sheet.Cells[row, 17+a].Text;
                    if (sheet.Cells[row, 17 + a].Value != null && sheet.Cells[row, 17 + a].Text == "1") tagsbool[a] = true;
                }
        }

        private static void buildOptions(int row)
        {
            List<string> x = null;
            for (int a = 0; a < 3; a++)
            {
                if (sheet.Cells[row, 10 + a * 2].Value != null)
                {
                    if (prodOptions[a].Values == null)
                    {
                        x = new List<string> { sheet.Cells[row, 10 + a * 2].Text };
                        prodOptions[a].Values = x;
                    }
                    else
                    {
                        
                        x = (List<string>)prodOptions[a].Values;
                        //only add if we dont have it
                        if (!x.Contains(sheet.Cells[row, 10 + a * 2].Text)) x.Add(sheet.Cells[row, 10 + a * 2].Text);
                        prodOptions[a].Values = x;
                    }
                }
            }
        }

        static async void UpdateCust(string bType, string etags)
        {
            var t = settags(bType);
            if (!String.IsNullOrEmpty(etags)) t = t + etags;
            var aproduct = await service.UpdateAsync((long)product.Id, new Product()
            {
                Tags = t,
                Options = prodOptions,
                Variants = product.Variants
            });
        }

    static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
              .SetBasePath(Directory.GetCurrentDirectory())
              .AddJsonFile("appsettings.json");
            //   .AddEnvironmentVariables();

            config = builder.Build();
            Console.WriteLine($"Website = {config["Shopify:CustomerWebsite"]}");
            Console.WriteLine($"Password = {config["Shopify:CustomerPassword"]}");
            service = new ProductService(config["Shopify:CustomerWebsite"], config["Shopify:CustomerPassword"]);


            Console.WriteLine("Opening Spreadsheet");
            var startRow = 2648; // Adjust these two to get all the rows. 
            var endRow = 2669;
            var inpath = "C:\\Mystuff\\PIM\\PIM Data Cleanup-V2.xlsx";
            var infile = new ExcelPackage(new FileInfo(inpath));

            var i = FindWorksheet(infile, "Data");
            string pTitle = "";
            
            product = null;
        //    IEnumerable<ProductOption> v_options = null;
            string v_bType = "";
            string esctags = "";

            if (i > 0)
            {
                sheet = infile.Workbook.Worksheets[i];
                for (var r = startRow; r <= endRow; r++)
                {
                    Console.WriteLine(sheet.Cells[r, 2].Text + " " + sheet.Cells[r, 5].Text + " " + sheet.Cells[r, 51].Text);


                    if(!((string)sheet.Cells[r, 5].Text == pTitle))
                    {
                        // finish current product
                        if (!(r == startRow))
                        {
                            UpdateCust(v_bType, esctags);
                           

                        }

                        // start new product
                        var filter = new ProductFilter()
                        {
                            Handle = (string)sheet.Cells[r, 51].Value
                        };

                        System.Threading.Thread.Sleep(1000);
                        var x = service.ListAsync(filter).Result.ToList();
                        product = x[0];
                        pTitle = sheet.Cells[r, 5].Text;

                        var eptitle = sheet.Cells[r, 6].Text + " " + sheet.Cells[r, 5].Text;
                        if (!String.IsNullOrEmpty(sheet.Cells[r, 8].Text)) eptitle = eptitle + " " + sheet.Cells[r, 8].Text;
                        if (product.Title != eptitle)
                        {
                            Console.WriteLine("SKU mismatch - havent found correct product" + sheet.Cells[r, 2].Text + " " + sheet.Cells[r, 5].Text);
                            Console.WriteLine("Found " + product.Title + " instead");

                            Console.ReadLine();
                            Environment.Exit(0);
                        }

                        //dealer discipline
                        v_bType = "";
                        v_bType = sheet.Cells[r, 50].Text;

                        //pick up the esc tags, e.g Esc$_ na_E$
                        esctags = "";
                        if (product.Tags.Contains("na_E$")) esctags = ",na_E$";
                        if (product.Tags.Contains("Esc$_"))
                        {
                            var pt = Regex.Match(product.Tags, @".Esc\$_(\d+),.");
                            esctags = esctags + ",Esc$_" + pt.Groups[1].Value;
                        }

                        for (int a = 0; a < 34; a++) tagsbool[a] = false;
                        for (int a = 0; a < 3; a++)
                        {
                            prodOptions[a] = new ProductOption();
                            prodOptions[a].ProductId = product.Id;
                            prodOptions[a].Position = a + 1;
                            if(sheet.Cells[r, 9 + a * 2].Value != null) prodOptions[a].Name = sheet.Cells[r, 9 + a * 2].Value.ToString();
                        }
                        if (String.IsNullOrEmpty(prodOptions[1].Name)) prodOptions[1] = null;
                        if (String.IsNullOrEmpty(prodOptions[2].Name)) prodOptions[2] = null;
                    }

                    //check what options are set on the row and update variable tagsbool
                    checkTags(r);
                    buildOptions(r);

                    foreach (var prodVariant in product.Variants)
                    {
                        if (prodVariant.SKU == sheet.Cells[r, 2].Value.ToString())
                        {
                            //found it so check options and update
                            prodVariant.Option1 = sheet.Cells[r, 10].Text;
                            prodVariant.Option2 = sheet.Cells[r, 12].Text;
                            prodVariant.Option3 = sheet.Cells[r, 14].Text;
                            break;
                        }
                            
                        
                    }

                }
            }

            Console.ReadLine();
        }
    }
}
